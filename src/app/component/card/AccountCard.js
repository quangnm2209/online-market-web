import React, { useEffect, useState } from "react";
import { Link } from "react-router-dom";
import AccountService from "../../service/AccountService";
import { toast } from "react-toastify";
import moment from "moment";
const AccountCard = ({ item, setReload, index }) => {
  const handleChangeStatus = async () => {
    try {
      const token = localStorage.getItem("token");
      if (token) {
        const data = await AccountService.changeStatus(item?.accountID);
        toast.success(data?.data?.message);
        setReload((pre) => !pre);
      }
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };
  return (
    <tr className="alert" role="alert">
      <td className="border-bottom-0-custom">{index + 1}</td>
      <td className="d-flex align-items-center border-bottom-0-custom">
        <div className="img">
          <img
            style={{ width: "45px", height: "45px", borderRadius: "50%" }}
            src={item?.avatar}
          />
        </div>
        <div className="pl-3 email">
          <span>
            <Link to={`/user/profile/${item?.accountID}`}>
              {item?.username}
            </Link>
          </span>
          <span></span>
        </div>
      </td>
      <td className="border-bottom-0-custom">{item?.roleName}</td>
      <td className="status border-bottom-0-custom">
        <span className={item?.status ? "active" : "inactiveColor"}>
          {item?.status ? "Hoạt động" : "không hoạt động"}
        </span>
      </td>
      <td className="border-bottom-0-custom">
        <button
          onClick={() => {
            handleChangeStatus();
          }}
          style={{ marginLeft: "5px", height: "30px", fontSize: "12px" }}
          type="button"
          className="btn btn-danger"
        >
          {item?.status ? "Khóa" : "Mở khóa"}
        </button>
      </td>
    </tr>
  );
};

export default AccountCard;
