import React, { useEffect, useRef, useState } from "react";
import { Carousel } from "react-bootstrap";
import "./style.scss";
import { Link, useNavigate } from "react-router-dom";
import { jwtDecode } from "jwt-decode";
import ContactService from "../service/ContactService";
const Home = () => {
  const navigate = useNavigate();

  const [role, setRole] = useState("");
  const [support, setSupport] = useState([]);

  const checkAdmin = (token) => {
    try {
      const decoded = jwtDecode(token);
      if (decoded?.role === "ADMIN") {
        navigate("/admin/dashboard");
      }
      setRole(decoded?.role);
    } catch (error) {
      return true; // Error in decoding or expired
    }
  };
  const getSupportList = async () => {
    try {
      const data = await ContactService.getContactList();
      setSupport(data);
    } catch (err) {
      console.log(err);
    }
  };

  useEffect(() => {
    const token = localStorage.getItem("token");
    if (token) {
      checkAdmin(token);
    }
    getSupportList();
  }, []);

  const supportRef = useRef(null);
  const scrollToSupport = () => {
    if (supportRef.current) {
      supportRef.current.scrollIntoView({ behavior: "smooth" });
    }
  };

  return (
    <div>
      <div>
        <header className="header-section">
          <div className="container">
            <Link
              style={{ marginTop: "-8px" }}
              className="link site-logo d-flex"
              to="/"
            >
              <img
                style={{
                  width: "50px",
                  height: "50px",
                  borderRadius: "50%",
                  marginRight: "5px",
                }}
                src="img/logo.png"
              />
              <h2
                style={{
                  display: "flex",
                  color: "white",
                  alignItems: "center",
                  fontSize: "25px",
                }}
              >
                Online{" "}
                <p
                  style={{
                    marginLeft: "3px",
                    color: "orange",
                    fontWeight: "700",
                    fontSize: "25px",
                    marginTop: "0",
                    marginBottom: "0px",
                    lineHeight: "0px",
                  }}
                >
                  Market
                </p>
              </h2>
            </Link>
            <div>
              <Link className="link" to="/login">
                <div style={{ color: "black" }} className="user-panel">
                  Đăng nhập
                </div>
              </Link>
            </div>
            <nav className="main-menu">
              <ul>
                {/* <li>
                  <a className="link" href="#support">
                    Tải xuống
                  </a>
                </li> */}
                <li>
                  <Link
                    className="link"
                    onClick={() => {
                      scrollToSupport();
                    }}
                  >
                    Liên hệ hỗ trợ
                  </Link>
                </li>
              </ul>
            </nav>
          </div>
        </header>
      </div>
      <HeroSection />
      <div ref={supportRef} className="footer_customer">
        <div>
          <h3 style={{ color: "white", fontSize: "18px", fontWeight: 700 }}>
            Liên hệ hỗ trợ
          </h3>
        </div>
        <div className="footer_supporter">
          {support?.map((item) => {
            if (item?.value?.includes("http")) {
              return (
                <div className="footer_item">
                  <div className="footer_left">{item?.name}: </div>
                  <a href={item?.value} target="_blank">
                    {item?.value}
                  </a>
                </div>
              );
            } else {
              return (
                <div className="footer_item">
                  <div className="footer_left">{item?.name}: </div>
                  <div>{item?.value}</div>
                </div>
              );
            }
          })}
        </div>
      </div>
    </div>
  );
};
const HeroSection = () => {
  return (
    <section className="hero-section">
      <Carousel>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="img/slider-1.jpg"
            alt="First slide"
          />
          <Carousel.Caption className="custom-caption">
            <div className="text-customer">
              <div className="container hs-text-custom">
                <h2>
                  Chợ <span>mới</span> độc nhất
                </h2>
                <p>Một hệ thống hỗ trợ cho các tiểu thương</p>
              </div>
            </div>
          </Carousel.Caption>
        </Carousel.Item>
        <Carousel.Item>
          <img
            className="d-block w-100"
            src="img/slider-2.jpg"
            alt="Second slide"
          />
          <Carousel.Caption className="custom-caption">
            <div className="text-customer">
              <div className="container hs-text-custom">
                <h2>
                  Rất nhiều <span>mặt hàng</span>
                </h2>
                <p>Có rất nhiều mặt hàng trên hệ thống.</p>
              </div>
            </div>
          </Carousel.Caption>
        </Carousel.Item>
      </Carousel>
    </section>
  );
};

export default Home;
