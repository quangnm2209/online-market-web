import React, { useContext, useEffect, useRef, useState } from "react";
import "./style.scss";
import { Link, useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import AuthService from "../service/AuthService";
import { UserContext } from "../../App";

const ForgotPassword = () => {
  const emailRef = useRef();

  const { setLoading } = useContext(UserContext);

  const [msg, setMsg] = useState({});

  const navigate = useNavigate();

  const handleForgotPassword = async () => {
    const user = {
      gmail: emailRef.current?.value,
    };
    let msgr = {};
    if (!user?.gmail) {
      msgr["gmail"] = "Email không được trống!";
    }
    if (msgr["gmail"]) {
      setMsg({ ...msgr });
      return;
    }
    try {
      setLoading(true);
      const data = await AuthService.forgotPassword(user.gmail);
      setLoading(false);
      toast.success(data?.data?.message);
      navigate("/login");
    } catch (err) {
      setLoading(false);
      toast.error(err?.response?.data?.message);
    }
  };
  return (
    <div className="auth">
      <section>
        <div className="leaves">
          <div className="set">
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454112/ag3hkiy77qyxixfjhcch.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454111/ahndh2nmrgau8nzlbtjq.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454110/p3hyhpkpqlhldre7mcec.png" />
            </div>
            <div>
              <img src="https://res.cloudinary.com/sttruyen/image/upload/v1719454209/i1d5l16g6choati8fsqd.png" />
            </div>
          </div>
        </div>
        <img src="img/slider-1.jpg" className="bg" />
        <img
          src="https://res.cloudinary.com/sttruyen/image/upload/v1719453179/kftjt59wpbrze906xlxe.gif"
          className="girl"
        />
        <img
          src="https://res.cloudinary.com/sttruyen/image/upload/v1719453124/wezwltak1pbcrxqwmgnv.gif"
          className="girl1"
        />
        <img
          src="https://res.cloudinary.com/sttruyen/image/upload/v1719453124/wezwltak1pbcrxqwmgnv.gif"
          className="bikerboy"
        />
        <div className="login">
          <h2>Quên mật khẩu</h2>
          <div className="inputBox">
            {msg["gmail"] && (
              <div style={{ color: "red", margin: "10px 0", fontSize: "14px" }}>
                * <i>{msg["gmail"]}</i>
              </div>
            )}
            <input type="text" ref={emailRef} placeholder="gmail" name="" />
          </div>
          <div className="inputBox">
            <input
              onClick={handleForgotPassword}
              type="submit"
              value="Quên mật khẩu"
              id="btn"
            />
          </div>
          <div style={{ justifyContent: "flex-end" }} className="group">
            <Link to="/login">Đăng nhập</Link>
          </div>
        </div>
      </section>
      {/* <HomeIcons /> */}
    </div>
  );
};

export default ForgotPassword;
