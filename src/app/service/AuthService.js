import axios from "axios";
import axiosInstance from "../component/axios_custom/AxiosCustom";

const AuthAPI = {
  login: async ({ user }) => {
    return await axios.post("/api/auth/common/admin/login", user);
  },
  createAccount: async (user) => {
    return await axiosInstance.post("/api/auth/admin/register", user);
  },
  forgotPassword: async (email) => {
    return await axios.put("/api/auth/common/admin/forgot-password", {
      email,
    });
  },
  ChangePassword: async (data) => {
    return await axiosInstance.put('/api/auth/all/change-password',data)
  }
};

export default AuthAPI;
