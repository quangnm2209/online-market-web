import axiosInstance from "../component/axios_custom/AxiosCustom";
const PaymentService = {
  getAll: async (search) => {
    if (!search) {
      search = "page=1&size=10";
    }
    return await axiosInstance.get(
      `/api/admin_and_sowner/registration-plans/request?${search}`
    );
  },
  review: async (data) => {
    return await axiosInstance.put(
      `/api/admin/registration-plans/package_request/review`,
      data
    );
  },
};

export default PaymentService;
