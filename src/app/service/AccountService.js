import axiosInstance from "../component/axios_custom/AxiosCustom";

const AccountService = {
  getAll: async (search) => {
    return await axiosInstance.get(
      `/api/admin/dashboard/account/list?${search}`
    );
  },
  changeStatus: async (id) => {
    return await axiosInstance.put(
      `/api/admin/dashboard/account/updateStatus/${id}`
    );
  },
  getProfile: async (id) => {
    return await axiosInstance.get(`/api/admin/dashboard/information/${id}`);
  },
};

export default AccountService;
