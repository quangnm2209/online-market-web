import axiosInstance from "../component/axios_custom/AxiosCustom";
const ThemeService = {
  create: async (data) => {
    return await axiosInstance.post(`/api/admin/dashboard/create/theme`, data);
  },
  getAll: async (param) => {
    return await axiosInstance.get(`/api/common/theme/list?${param}`);
  },
  update: async (data) => {
    return await axiosInstance.put(`/api/admin/dashboard/update/theme`, data);
  },
  delete: async (id) => {
    return await axiosInstance.delete(
      `/api/admin/dashboard/delete/theme/${id}`
    );
  },
};

export default ThemeService;
