import axiosInstance from "../component/axios_custom/AxiosCustom";

const CategoryService = {
  getAll: async (search) => {
    if (!search) {
      search = "page=1&size=10";
    }
    return await axiosInstance.get(`/api/admin/categories/getAll?${search}`);
  },
  create: async (category) => {
    return await axiosInstance.post("/api/admin/categories/add", category);
  },
  update: async (id, category) => {
    return await axiosInstance.put(
      `/api/admin/categories/update/${id}`,
      category
    );
  },
  changeStatus: async (id) => {
    return await axiosInstance.put(`/api/admin/categories/updateStatus/${id}`);
  },
  delete: async (id) => {
    return await axiosInstance.delete(`/api/admin/categories/delete/${id}`);
  },
};
export default CategoryService;
