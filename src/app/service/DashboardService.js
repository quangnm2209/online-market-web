import axiosInstance from "../component/axios_custom/AxiosCustom";
const DashboardService = {
  getDashboardTotal: async () => {
    try {
      const response = await axiosInstance.get(`/api/admin/dashboard/details`);
      return response.data;
    } catch (err) {
      throw err;
    }
  },
  getMonth: async (month, year) => {
    try {
      if (!month) {
        month = new Date().getMonth() + 1;
      }
      if (!year) {
        year = new Date().getFullYear();
      }
      const response = await axiosInstance.get(
        `/api/admin/dashboard/details/current/month?month=${month}&year=${year}`
      );
      return response.data;
    } catch (err) {
      throw err;
    }
  },
};

export default DashboardService;
