import axiosInstance from "../component/axios_custom/NodeJsAxiosCustom";
const ContactService = {
  getContactList: async () => {
    try {
      const response = await axiosInstance.get("/contact");
      return response.data;
    } catch (err) {
      throw err;
    }
  },
  createContact: async (data) => {
    try {
      const response = await axiosInstance.post("/contact/create", data);
      return response.data;
    } catch (err) {
      throw err;
    }
  },
  updateContact: async (id, data) => {
    try {
      const response = await axiosInstance.put(`/contact/update/${id}`, data);
      return response.data;
    } catch (err) {
      throw err;
    }
  },
  deleteContact: async (id) => {
    try {
      const response = await axiosInstance.delete(`/contact/delete/${id}`);
      return response.data;
    } catch (err) {
      throw err;
    }
  },
};

export default ContactService;
