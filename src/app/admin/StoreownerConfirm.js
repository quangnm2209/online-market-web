import React, { useCallback, useEffect, useRef, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import { toast } from "react-toastify";
import BeingService from "../service/BeingService";
import Select from "react-select";
import Pagination from "../component/pagination/Pagination";

const options = [
  { value: "DATE_DECREASE", label: "Mới nhất" },
  { value: "DATE_INCREASE", label: "Cũ nhất" },
];

const typeOptions = [
  { value: "", label: "Tất cả" },
  { value: "STORE_OWNER_PENDING", label: "Đợi xác nhận" },
  { value: "STORE_OWNER_APPROVED", label: "Đã xác nhận" },
  { value: "STORE_OWNER_REJECTED", label: "Đã từ chối" },
];
const customStyles = {
  control: (provided) => ({
    ...provided,
    minHeight: "34px",
    borderRadius: "20px", // Set the height here
  }),
  valueContainer: (provided) => ({
    ...provided,
    height: "34px", // Ensures the container aligns with the height of the control
    padding: "0 6px",
  }),
  input: (provided) => ({
    ...provided,
    margin: "0px",
  }),
  indicatorSeparator: (provided) => ({
    display: "none", // Hide the separator
  }),
  indicatorsContainer: (provided) => ({
    ...provided,
    height: "34px", // Ensures the container aligns with the height of the control
  }),
};

const StoreownerConfirm = () => {
  const [requests, setRequests] = useState([]);
  const [totalPage, setTotalPage] = useState(1);
  const [current, setCurrent] = useState(null);
  const [cancel, setCancel] = useState(null);
  const [reload, setReload] = useState(false);

  const { search } = useLocation();
  const getQueryParam = (name) => {
    const searchParams = new URLSearchParams(search);
    return searchParams.get(name) || (name == "page" ? "1" : "");
  };

  const searchRef = useRef();
  const dateRef = useRef();

  const handleGetAllRequest = async (url) => {
    try {
      const data = await BeingService.getAll(url);
      setRequests(data?.data?.datas);
      setTotalPage(data?.data?.totalPage);
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    handleSearch();
  }, [search, reload]);

  const handleCancelRequest = useCallback((item) => {
    setCurrent(null);
    setCancel(item);
  });

  const [selectedOption, setSelectedOption] = useState(options[0]);

  const handleChange = (option) => {
    setSelectedOption(option);
  };

  const [selectedTypeOption, setSelectedTypeOption] = useState(typeOptions[0]);

  const handleTypeChange = (option) => {
    setSelectedTypeOption(option);
  };

  const handleSearch = () => {
    const filteredSearch = {
      search: searchRef.current.value,
      sort: selectedOption?.value,
      createAt: dateRef.current.value,
      page: getQueryParam("page"),
      size: 10,
      status: selectedTypeOption?.value,
    };
    const excludesFields = ["search", "sort", "createAt", "status"];
    excludesFields?.forEach((item) => {
      if (!filteredSearch[item]) {
        delete filteredSearch[item];
      }
    });
    const searchURL = new URLSearchParams(filteredSearch);
    handleGetAllRequest(`${searchURL}`);
  };

  return (
    <div>
      <div
        style={{
          display: "flex",
          marginTop: "20px",
          justifyContent: "space-between",
        }}
      ></div>
      <div className="search_form">
        <div className="search_form_wrap">
          <div className="search_form_item">
            <input
              ref={searchRef}
              className="search_form_input"
              placeholder="Tìm kiếm..."
            />
          </div>
          <div className="search_form_item">
            <label style={{ marginRight: "10px" }}>Sắp xếp: </label>
            <Select
              styles={customStyles}
              value={selectedOption}
              onChange={handleChange}
              options={options}
            />
          </div>
          <div className="search_form_item">
            <label style={{ marginRight: "10px" }}>Loại: </label>
            <Select
              styles={customStyles}
              value={selectedTypeOption}
              onChange={handleTypeChange}
              options={typeOptions}
            />
          </div>
          <div className="search_form_item">
            <label style={{ marginRight: "10px" }}>Ngày gửi yêu cầu:</label>
            <input ref={dateRef} type="date" className="search_form-date" />
          </div>
          <div>
            <div>
              <button
                onClick={handleSearch}
                className="btn btn-primary"
                style={{
                  borderRadius: "40px",
                  height: "34px",
                  fontSize: "13px",
                }}
              >
                Tìm kiếm
              </button>
            </div>
          </div>
        </div>
      </div>
      {(!requests || requests?.length == 0) && (
        <div style={{ textAlign: "center", width: "100%" }}>
          <p style={{ fontSize: "16px", color: "red" }}>
            (Không tìm thấy dữ liệu)
          </p>
        </div>
      )}
      <section style={{ marginTop: "15px" }} className="ftco-section">
        <div className="row">
          <div className="col-md-12">
            <div className="table-wrap">
              <table className="table table-responsive-xl">
                <thead>
                  <tr>
                    <th style={{ width: "10%" }}>ID</th>
                    <th style={{ width: "25%" }}>Người gửi</th>
                    <th style={{ width: "15%" }}>Ngày gửi yêu cầu</th>
                    <th style={{ width: "15%" }}>Trạng thái</th>
                    <th style={{ width: "15%" }}>Lý do</th>
                    <th style={{ width: "20%" }}>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  {requests?.map((item, index) => (
                    <TableRow
                      index={index}
                      setCurrent={setCurrent}
                      item={item}
                      key={item?.roleRegistryID}
                      handleCancelRequest={handleCancelRequest}
                    />
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
      {current && (
        <Infor
          current={current}
          setCurrent={setCurrent}
          handleCancelRequest={handleCancelRequest}
          setReload={setReload}
        />
      )}
      {cancel && (
        <CancelRequest
          current={cancel}
          setCurrent={setCancel}
          setReload={setReload}
        />
      )}
      <Pagination count={totalPage} />
    </div>
  );
};
const TableRow = ({ item, setCurrent, handleCancelRequest, index }) => {
  const [currentStatus, setCurrentStatus] = useState("");
  const [status, setStatus] = useState("");
  useEffect(() => {
    if (item?.reviewStatus) {
      switch (item?.reviewStatus) {
        case "STORE_OWNER_REJECTED":
          setCurrentStatus("inactiveColor");
          setStatus("Đã từ chối");
          break;
        case "STORE_OWNER_APPROVED":
          setCurrentStatus("active");
          setStatus("Đã xác nhận");
          break;
        default:
          setCurrentStatus("pendingColor");
          setStatus("Đợi xác nhận");
          break;
      }
    }
  }, [item]);

  return (
    <tr className="alert" role="alert">
      <td className="border-bottom-0-custom">{index + 1}</td>
      <td className="d-flex align-items-center border-bottom-0-custom">
        <div className="img">
          <img
            style={{
              width: "45px",
              height: "45px",
              borderRadius: "50%",
              objectFit: "cover",
            }}
            src={item?.avatar}
          />
        </div>
        <div className="pl-3 email">
          <span>
            <Link to={`/user/profile/${item?.accountID}`}>
              {item?.username}
            </Link>
          </span>
          <span>{item?.storeName}</span>
        </div>
      </td>
      <td className="border-bottom-0-custom">{item?.reviewCreateDate}</td>
      <td className="status border-bottom-0-custom">
        <span className={currentStatus}>{status}</span>
      </td>
      <td className="border-bottom-0-custom">{item?.rejectReason}</td>
      <td className="border-bottom-0-custom">
        <button
          onClick={() => {
            setCurrent(item);
          }}
          style={{ height: "30px", fontSize: "12px" }}
          type="button"
          className="btn btn-danger"
        >
          Thông tin
        </button>
        {currentStatus == "pendingColor" && (
          <button
            onClick={() => {
              handleCancelRequest(item);
            }}
            style={{
              marginLeft: "5px",
              height: "30px",
              fontSize: "12px",
            }}
            type="button"
            className="btn btn-secondary"
          >
            Hủy yêu cầu
          </button>
        )}
      </td>
    </tr>
  );
};
const Infor = ({ current, setCurrent, handleCancelRequest, setReload }) => {
  const [currentStatus, setCurrentStatus] = useState("");
  useEffect(() => {
    if (current?.reviewStatus) {
      switch (current?.reviewStatus) {
        case "STORE_OWNER_REJECTED":
          setCurrentStatus("inactiveColor");
          break;
        case "STORE_OWNER_APPROVED":
          setCurrentStatus("active");
          break;
        default:
          setCurrentStatus("pendingColor");
          break;
      }
    }
  }, [current]);
  const handleApproveRequest = async () => {
    try {
      const request = {
        roleRegistryID: current?.roleRegistryID,
        adminReviewStatus: true,
        rejectReason: "",
      };
      const data = await BeingService.reviewRequest(request);
      toast.success(data?.data?.message);
      setReload((pre) => !pre);
      setCurrent(null);
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };
  return (
    <div className="create_container">
      <div className="infor_wrap">
        <div
          onClick={() => {
            setCurrent(null);
          }}
          className="create_close_wrap"
        >
          <i className="fa-solid fa-x"></i>
        </div>
        <div
          style={{
            display: "flex",
            justifyContent: "center",
            flexWrap: "wrap",
            alignItems: "center",
            flexDirection: "column",
          }}
        >
          <div className="infor_img_wrap">
            <img src={current?.storeImage} />
          </div>
          <div
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
            <div className="infor_detail_wrap">
              <div>Số điện thoại:</div>
              <div className="storeName">{current?.storePhone}</div>
            </div>
            <div className="infor_detail_wrap">
              <div>Địa chỉ:</div>
              <div className="storeName">{current?.storeName}</div>
            </div>
          </div>
        </div>
        {currentStatus == "pendingColor" && (
          <div className="infor_btn_wrap">
            <button onClick={handleApproveRequest} className="btn btn-primary">
              Đồng ý
            </button>
            <button
              onClick={() => {
                handleCancelRequest(current);
              }}
              style={{ marginLeft: 10 }}
              className="btn btn-danger"
            >
              Hủy yêu cầu
            </button>
          </div>
        )}
      </div>
    </div>
  );
};

const CancelRequest = ({ current, setCurrent, setReload }) => {
  const reasonRef = useRef(null);

  const [error, setError] = useState(false);

  const handleRefuseRequest = async () => {
    try {
      if (!reasonRef.current?.value) {
        setError(true);
        return;
      }
      setError(false);
      const request = {
        roleRegistryID: current?.roleRegistryID,
        adminReviewStatus: false,
        rejectReason: reasonRef.current?.value,
      };
      const data = await BeingService.reviewRequest(request);
      toast.success(data?.data?.message);
      setReload((pre) => !pre);
      setCurrent(null);
    } catch (err) {
      setError(true);
      toast.error(err?.response?.data?.message);
    }
  };

  return (
    <div className="create_container">
      <div className="infor_wrap">
        <div
          onClick={() => {
            setCurrent(null);
          }}
          className="create_close_wrap"
        >
          <i className="fa-solid fa-x"></i>
        </div>
        <div style={{ textAlign: "center", marginBottom: 30 }}>
          <h2 style={{ fontSize: "19px", fontWeight: "700" }}>
            Lý do hủy yêu cầu của {current?.storeName}
          </h2>
        </div>
        {error && (
          <div
            style={{ textAlign: "center", color: "red", marginBottom: "-10px" }}
          >
            <i>Lý do không được bỏ trống!</i>
          </div>
        )}
        <div className="reason_input">
          <textarea ref={reasonRef} placeholder="Lý do *" />
        </div>
        <div className="infor_btn_wrap">
          <button onClick={handleRefuseRequest} className="btn btn-primary">
            Đồng ý
          </button>
          <button
            onClick={() => {
              setCurrent(null);
            }}
            style={{ marginLeft: 10 }}
            className="btn btn-danger"
          >
            Hủy
          </button>
        </div>
      </div>
    </div>
  );
};

export default StoreownerConfirm;
