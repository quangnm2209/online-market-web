import React, { useContext, useEffect, useRef, useState } from "react";
import { useLocation, useNavigate } from "react-router-dom";
import Pagination from "../component/pagination/Pagination";
import AccountCard from "../component/card/AccountCard";
import { toast } from "react-toastify";
import AuthService from "../service/AuthService";
import AccountService from "../service/AccountService";
import Select from "react-select";
import ImageService from "../service/ImageService";
import { UserContext } from "../../App";

const options = [
  { value: "DATE_DECREASE", label: "Mới nhất" },
  { value: "DATE_INCREASE", label: "Cũ nhất" },
];
const statusOptions = [
  { value: "all", label: "Tất cả" },
  { value: false, label: "Không hoạt động" },
  { value: true, label: "Hoạt động" },
];
const typeOptions = [
  { value: "", label: "Tất cả" },
  { value: "ADMIN", label: "Admin" },
  { value: "CUSTOMER", label: "Khách hàng" },
  { value: "STORE_OWNER", label: "Tiểu thương" },
];
const customStyles = {
  control: (provided) => ({
    ...provided,
    minHeight: "34px",
    borderRadius: "20px", // Set the height here
  }),
  valueContainer: (provided) => ({
    ...provided,
    height: "34px",
    padding: "0 6px",
  }),
  input: (provided) => ({
    ...provided,
    margin: "0px",
  }),
  indicatorSeparator: (provided) => ({
    display: "none", // Hide the separator
  }),
  indicatorsContainer: (provided) => ({
    ...provided,
    height: "34px",
  }),
};
const Accounts = () => {
  const [accounts, setAccounts] = useState([]);
  const [totalPage, setTotalPage] = useState(1);

  const searchRef = useRef();

  const { search } = useLocation();
  const getQueryParam = (name) => {
    const searchParams = new URLSearchParams(search);
    return searchParams.get(name) || (name == "page" ? "1" : "");
  };

  const [reload, setReload] = useState(false);
  const [showAdd, setShowAdd] = useState(false);

  useEffect(() => {
    handleSearch();
  }, [reload, search]);

  const handleGetAllAccounts = async (query) => {
    try {
      const data = await AccountService.getAll(query);
      setAccounts(data?.data?.datas);
      setTotalPage(data?.data?.totalPage);
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };

  const [selectedOption, setSelectedOption] = useState(options[0]);

  const handleChange = (option) => {
    setSelectedOption(option);
  };

  const [selectedTypeOption, setSelectedTypeOption] = useState(typeOptions[0]);

  const handleTypeChange = (option) => {
    setSelectedTypeOption(option);
  };

  const [selectedstatusOption, setSelectedstatusOption] = useState(
    statusOptions[0]
  );

  const handlestatusChange = (option) => {
    setSelectedstatusOption(option);
  };

  const handleSearch = () => {
    const filteredSearch = {
      search: searchRef.current.value,
      sort: selectedOption?.value,
      page: getQueryParam("page"),
      size: 10,
      role: selectedTypeOption?.value,
      status: selectedstatusOption?.value,
    };
    if (filteredSearch.status == "all") {
      delete filteredSearch.status;
    }
    const excludesFields = ["search", "sort", "role"];
    excludesFields?.forEach((item) => {
      if (!filteredSearch[item]) {
        delete filteredSearch[item];
      }
    });
    const searchURL = new URLSearchParams(filteredSearch);
    handleGetAllAccounts(`${searchURL}`);
  };

  return (
    <div>
      <div
        style={{
          display: "flex",
          marginTop: "20px",
          justifyContent: "flex-end",
        }}
      >
        <button
          onClick={() => {
            setShowAdd(true);
          }}
          className="btn btn-primary"
        >
          Thêm mới tk admin
        </button>
      </div>
      <div className="search_form">
        <div className="search_form_wrap">
          <div className="search_form_item">
            <input
              ref={searchRef}
              className="search_form_input"
              placeholder="Tìm kiếm..."
            />
          </div>
          <div className="search_form_item">
            <label style={{ marginRight: "10px" }}>Sắp xếp: </label>
            <Select
              styles={customStyles}
              value={selectedOption}
              onChange={handleChange}
              options={options}
            />
          </div>
          <div className="search_form_item">
            <label style={{ marginRight: "10px" }}>Vai trò: </label>
            <Select
              styles={customStyles}
              value={selectedTypeOption}
              onChange={handleTypeChange}
              options={typeOptions}
            />
          </div>
          <div className="search_form_item">
            <label style={{ marginRight: "10px" }}>Trạng thái: </label>
            <Select
              styles={customStyles}
              value={selectedstatusOption}
              onChange={handlestatusChange}
              options={statusOptions}
            />
          </div>
          <div>
            <div>
              <button
                onClick={handleSearch}
                className="btn btn-primary"
                style={{
                  borderRadius: "40px",
                  height: "34px",
                  fontSize: "13px",
                }}
              >
                Tìm kiếm
              </button>
            </div>
          </div>
        </div>
      </div>
      <section style={{ marginTop: "15px" }} className="ftco-section">
        <div className="row">
          <div className="col-md-12">
            <div className="table-wrap">
              <table className="table table-responsive-xl">
                <thead>
                  <tr>
                    <th>ID</th>
                    <th>Thông tin</th>
                    <th>Vai trò</th>
                    <th>Trạng thái</th>
                    <th>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  {accounts?.map((item, index) => (
                    <AccountCard
                      index={index}
                      key={item?.accountID}
                      setReload={setReload}
                      item={item}
                    />
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
      {showAdd && <AddNewAccount setShow={setShowAdd} setReload={setReload} />}

      <Pagination count={totalPage} />
    </div>
  );
};
const AddNewAccount = ({ setShow, setReload }) => {
  const [err, setErr] = useState({
    username: "",
    email: "",
    password: "",
  });
  const { setLoading } = useContext(UserContext);
  const usernameRef = useRef();
  const emailRef = useRef();
  const passwordRef = useRef();

  const handleCreateNewAccount = async () => {
    try {
      const user = {
        username: usernameRef.current.value,
        email: emailRef.current.value,
        password: passwordRef.current.value,
      };
      const userFeildName = {
        username: "Tên",
        email: "Email",
        password: "Mật khẩu",
      };
      let isErr = false;
      let newError = {};
      for (const [key, value] of Object.entries(user)) {
        if (!user[key]) {
          isErr = true;
          newError[key] = userFeildName[key] + " không được bỏ trống!";
        }
      }
      setErr(newError);

      if (!isErr) {
        setLoading(true);
        const data = await AuthService.createAccount(user);
        toast.success(data?.data?.message);
        setShow(false);
        setReload((pre) => !pre);
        setLoading(false);
      }
    } catch (err) {
      setLoading(false);
      setErr({
        ...err?.response?.data,
      });
      toast.error(err?.response?.data?.message);
    }
  };

  return (
    <div className="create_container">
      <div className="create_wrap">
        <div
          onClick={() => {
            setShow(false);
          }}
          className="create_close_wrap"
        >
          <i className="fa-solid fa-x"></i>
        </div>
        <div style={{ marginBottom: "20px", marginTop: "30px" }}>
          {err?.username && (
            <div className="create_input_container">
              <i
                style={{
                  color: "red",
                  marginBottom: "0px",
                  marginTop: "15px",
                }}
              >
                {err?.username}
              </i>
            </div>
          )}
          <div className="create_input_container">
            <input
              ref={usernameRef}
              placeholder="Tên *"
              className="create_input"
              name="username"
            />
          </div>
          {err?.email && (
            <div className="create_input_container">
              <i
                style={{
                  color: "red",
                  marginBottom: "-20px",
                  marginTop: "15px",
                }}
              >
                {err?.email}
              </i>
            </div>
          )}
          <div style={{ marginTop: "20px" }} className="create_input_container">
            <input
              ref={emailRef}
              placeholder="Email *"
              className="create_input"
              name="email"
            />
          </div>
          {err?.password && (
            <div className="create_input_container">
              <i
                style={{
                  color: "red",
                  marginBottom: "-20px",
                  marginTop: "15px",
                }}
              >
                {err?.password}
              </i>
            </div>
          )}
          <div
            style={{ marginTop: "20px", position: "relative" }}
            className="create_input_container"
          >
            <input
              ref={passwordRef}
              placeholder="Mật khẩu *"
              className="create_input"
              type="password"
            />
            <div
              onClick={() => {
                if (passwordRef.current) {
                  if (passwordRef.current.type === "text") {
                    passwordRef.current.type = "password";
                  } else {
                    passwordRef.current.type = "text";
                  }
                }
              }}
              className="eyes_items_custom"
            >
              <i className="fa-solid fa-eye"></i>
            </div>
          </div>
          <div className="btn_create_container">
            <button
              onClick={handleCreateNewAccount}
              style={{ margin: "0 5px" }}
              className="btn btn-primary"
            >
              Tạo mới
            </button>
            <button
              onClick={() => {
                setShow(false);
              }}
              style={{ margin: "0 5px" }}
              className="btn btn-secondary"
            >
              Hủy
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Accounts;
