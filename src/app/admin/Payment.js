import React, { useCallback, useEffect, useRef, useState } from "react";
import { Link, useLocation } from "react-router-dom";
import CommonService from "../service/CommonService";
import { toast } from "react-toastify";
import PaymentService from "../service/PaymentService";
import Select from "react-select";
import Pagination from "../component/pagination/Pagination";
import moment from "moment";
const options = [
  { value: "DATE_DECREASE", label: "Mới nhất" },
  { value: "DATE_INCREASE", label: "Cũ nhất" },
];

const typeOptions = [
  { value: "", label: "Tất cả" },
  { value: "REGISTRY_PENDING", label: "Đợi xác nhận" },
  { value: "APPROVED", label: "Đã xác nhận" },
  { value: "REJECTED", label: "Đợi từ chối" },
];
const customStyles = {
  control: (provided) => ({
    ...provided,
    minHeight: "34px",
    borderRadius: "20px", // Set the height here
  }),
  valueContainer: (provided) => ({
    ...provided,
    height: "34px", // Ensures the container aligns with the height of the control
    padding: "0 6px",
  }),
  input: (provided) => ({
    ...provided,
    margin: "0px",
  }),
  indicatorSeparator: (provided) => ({
    display: "none", // Hide the separator
  }),
  indicatorsContainer: (provided) => ({
    ...provided,
    height: "34px", // Ensures the container aligns with the height of the control
  }),
};
const Payment = () => {
  const [payments, setPayments] = useState([]);
  const [current, setCurrent] = useState(null);
  const [totalPage, setTotalPage] = useState(1);
  const [reload, setReload] = useState(false);

  const { search } = useLocation();
  const getQueryParam = (name) => {
    const searchParams = new URLSearchParams(search);
    return searchParams.get(name) || (name == "page" ? "1" : "");
  };

  const searchRef = useRef();
  const dateRef = useRef();

  const handleGetAllPayment = async (search) => {
    try {
      const data = await PaymentService.getAll(search);

      setPayments(data?.data?.datas);
      setTotalPage(data?.data?.totalPage);
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };
  useEffect(() => {
    handleSearch();
  }, [reload, search]);

  const handleRejected = useCallback((item) => {
    setCurrent(item);
  });

  const [selectedOption, setSelectedOption] = useState(options[0]);

  const handleChange = (option) => {
    setSelectedOption(option);
  };

  const [selectedTypeOption, setSelectedTypeOption] = useState(typeOptions[0]);

  const handleTypeChange = (option) => {
    setSelectedTypeOption(option);
  };

  const handleSearch = () => {
    const filteredSearch = {
      search: searchRef.current.value,
      sort: selectedOption?.value,
      createAt: dateRef.current.value,
      page: getQueryParam("page"),
      size: 10,
      status: selectedTypeOption?.value,
    };
    const excludesFields = ["search", "sort", "createAt", "status"];
    excludesFields?.forEach((item) => {
      if (!filteredSearch[item]) {
        delete filteredSearch[item];
      }
    });
    const searchURL = new URLSearchParams(filteredSearch);
    handleGetAllPayment(searchURL.toString());
  };

  return (
    <div>
      <div
        style={{
          display: "flex",
          marginTop: "20px",
          justifyContent: "space-between",
        }}
      ></div>
      <div className="search_form">
        <div className="search_form_wrap">
          <div className="search_form_item">
            <input
              ref={searchRef}
              className="search_form_input"
              placeholder="Tìm kiếm..."
            />
          </div>
          <div className="search_form_item">
            <label style={{ marginRight: "10px" }}>Sắp xếp: </label>
            <Select
              styles={customStyles}
              value={selectedOption}
              onChange={handleChange}
              options={options}
            />
          </div>
          <div className="search_form_item">
            <label style={{ marginRight: "10px" }}>Loại: </label>
            <Select
              styles={customStyles}
              value={selectedTypeOption}
              onChange={handleTypeChange}
              options={typeOptions}
            />
          </div>
          <div className="search_form_item">
            <label style={{ marginRight: "10px" }}>Ngày gửi yêu cầu:</label>
            <input ref={dateRef} type="date" className="search_form-date" />
          </div>
          <div>
            <div>
              <button
                onClick={handleSearch}
                className="btn btn-primary"
                style={{
                  borderRadius: "40px",
                  height: "34px",
                  fontSize: "13px",
                }}
              >
                Tìm kiếm
              </button>
            </div>
          </div>
        </div>
      </div>
      {(!payments || payments?.length == 0) && (
        <div style={{ textAlign: "center", width: "100%" }}>
          <p style={{ fontSize: "16px", color: "red" }}>
            (Không tìm thấy dữ liệu)
          </p>
        </div>
      )}
      <section style={{ marginTop: "15px" }} className="ftco-section">
        <div className="row">
          <div className="col-md-12">
            <div className="table-wrap">
              <table className="table table-responsive-xl">
                <thead>
                  <tr>
                    <th style={{ width: "2%" }}>ID</th>
                    <th style={{ width: "15%" }}>Người gửi</th>
                    <th style={{ width: "10%" }}>Ngày gửi yêu cầu</th>
                    <th style={{ width: "10%" }}>Ngày xác nhận đơn</th>
                    <th style={{ width: "10%" }}>Gói thanh toán</th>
                    <th style={{ width: "10%" }}>Giá mua</th>
                    <th style={{ width: "13%" }}>Lý do</th>
                    <th style={{ width: "15%" }}>Trạng thái</th>
                    <th style={{ width: "20%" }}>&nbsp;</th>
                  </tr>
                </thead>
                <tbody>
                  {payments?.map((item, index) => (
                    <PaymentItem
                      item={item}
                      key={item?.registrationID}
                      handleRejected={handleRejected}
                      setReload={setReload}
                      index={index}
                    />
                  ))}
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </section>
      {current && (
        <CancelRequest
          setReload={setReload}
          current={current}
          setCurrent={setCurrent}
        />
      )}

      <Pagination count={totalPage} />
    </div>
  );
};
const PaymentItem = ({ item, handleRejected, setReload, index }) => {
  const [currentStatus, setCurrentStatus] = useState("");
  const [status, setStatus] = useState("");
  useEffect(() => {
    if (item?.reviewStatus) {
      switch (item?.reviewStatus) {
        case "REGISTRY_PENDING":
          setCurrentStatus("pendingColor");
          setStatus("Đợi xác nhận");
          break;
        case "APPROVED":
          setCurrentStatus("active");
          setStatus("Đã xác nhận");
          break;
        case "REJECTED":
          setCurrentStatus("inactiveColor");
          setStatus("Đã từ chối");
          break;
      }
    }
  }, [item]);
  const handleApproveRequest = async () => {
    try {
      const request = {
        registrationID: item?.registrationID,
        adminReviewStatus: true,
        rejectReason: "",
      };
      const data = await PaymentService.review(request);
      toast.success(data?.data?.message);
      setReload((pre) => !pre);
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };
  return (
    <tr className="alert" role="alert">
      <td className="border-bottom-0-custom">{index + 1}</td>
      <td className="d-flex align-items-center border-bottom-0-custom">
        <div className="img">
          <img
            style={{
              width: "45px",
              height: "45px",
              borderRadius: "50%",
              objectFit: "cover",
            }}
            src={item?.avatar}
          />
        </div>
        <div className="pl-3 email">
          <span>
            <Link to={`/user/profile/${item?.accountID}`}>
              {item?.username}
            </Link>
          </span>
          <span>{item?.phoneNumber}</span>
        </div>
      </td>
      <td className="border-bottom-0-custom">
        {moment(item?.reviewCreateDate).fromNow()}
      </td>
      <td className="border-bottom-0-custom">
        {moment(item?.reviewEndDate).fromNow()}
      </td>
      <td className="border-bottom-0-custom">{item?.planName}</td>
      <td className="border-bottom-0-custom">
        {CommonService.formatNumberWithDots(item?.planPrice)} VND
      </td>
      <td className="border-bottom-0-custom">{item?.rejectReason}</td>
      <td className="status border-bottom-0-custom">
        <span className={currentStatus}>{status}</span>
      </td>
      {currentStatus == "pendingColor" && (
        <td className="border-bottom-0-custom">
          <button
            onClick={handleApproveRequest}
            style={{ height: "30px", fontSize: "12px" }}
            type="button"
            className="btn btn-danger"
          >
            Xác nhận
          </button>
          <button
            onClick={() => {
              handleRejected(item);
            }}
            style={{
              marginLeft: "5px",
              height: "30px",
              fontSize: "12px",
            }}
            type="button"
            className="btn btn-secondary"
          >
            Từ chối
          </button>
        </td>
      )}
    </tr>
  );
};
const CancelRequest = ({ current, setCurrent, setReload }) => {
  const reasonRef = useRef(null);

  const [error, setError] = useState(false);

  const handleRefuseRequest = async () => {
    if (!reasonRef.current?.value) {
      setError(true);
      return;
    }
    try {
      const request = {
        registrationID: current?.registrationID,
        adminReviewStatus: false,
        rejectReason: reasonRef.current?.value,
      };
      const data = await PaymentService.review(request);
      toast.success(data?.data?.message);
      setReload((pre) => !pre);
      setCurrent(null);
    } catch (err) {
      setError(true);
      toast.error(err?.response?.data?.message);
    }
  };

  return (
    <div className="create_container">
      <div className="infor_wrap">
        <div
          onClick={() => {
            setCurrent(null);
          }}
          className="create_close_wrap"
        >
          <i className="fa-solid fa-x"></i>
        </div>
        <div style={{ textAlign: "center", marginBottom: 30 }}>
          <h2 style={{ fontSize: "19px", fontWeight: "700" }}>
            Lý do hủy yêu cầu của {current?.username}
          </h2>
        </div>
        {error && (
          <div
            style={{ textAlign: "center", color: "red", marginBottom: "-10px" }}
          >
            <i>Lý do không được bỏ trống!</i>
          </div>
        )}
        <div className="reason_input">
          <textarea ref={reasonRef} placeholder="Lý do *" />
        </div>
        <div className="infor_btn_wrap">
          <button onClick={handleRefuseRequest} className="btn btn-primary">
            Đồng ý
          </button>
          <button
            onClick={() => {
              setCurrent(null);
            }}
            style={{ marginLeft: 10 }}
            className="btn btn-danger"
          >
            Hủy
          </button>
        </div>
      </div>
    </div>
  );
};

export default Payment;
