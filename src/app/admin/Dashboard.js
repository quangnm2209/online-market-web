import React, { useEffect, useRef, useState } from "react";
import {
  Chart as ChartJS,
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend,
} from "chart.js";
import { Bar } from "react-chartjs-2";
import { useNavigate } from "react-router-dom";
import { toast } from "react-toastify";
import DashboardService from "../service/DashboardService";
import CommonService from "../service/CommonService";
ChartJS.register(
  CategoryScale,
  LinearScale,
  BarElement,
  Title,
  Tooltip,
  Legend
);
const Dashboard = () => {
  const [dashboard, setDashboard] = useState({});
  const [months, setMonths] = useState({});

  const [selectMonth, setSelectMonth] = useState("");
  const navigate = useNavigate();
  const monthRef = useRef();

  const [todayData, setTodayData] = useState({
    selling: 0,
    revenue: 0,
  });

  useEffect(() => {
    handleGetDashboard();
  }, []);

  const handleGetDashboard = async () => {
    try {
      const dashboardList = [
        await DashboardService.getDashboardTotal(),
        await DashboardService.getMonth(),
      ];
      const currentDate = new Date();
      const year = currentDate.getFullYear();
      const month = String(currentDate.getMonth() + 1).padStart(2, "0");
      setSelectMonth(`${year}-${month}`);
      const datas = await Promise.allSettled(dashboardList);
      setDashboard(datas[0]?.value);
      setMonths(datas[1]?.value);

      let day = new Date().getDate();
      if (datas[1]?.status == "fulfilled") {
        let newTodayData = {
          revenue: datas[1]?.value?.totalRegistrationAmount[day - 1] || 0,
          selling: datas[1]?.value?.totalRegistrations[day - 1] || 0,
        };
        setTodayData(newTodayData);
      }
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };
  const handleSearchMonth = async () => {
    try {
      const value = monthRef.current.value;
      if (value) {
        const [year, month] = value.split("-");
        const datas = await DashboardService.getMonth(month, year);
        setMonths(datas);
      }
    } catch (err) {
      toast.error(err?.response?.data?.message);
    }
  };

  let date = 31;
  let labels = Array(date)
    .fill(1)
    .map((_, index) => index + 1);
  let options = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
      },
      title: {
        display: true,
        text: "Thống kê tài khoản",
      },
    },
  };

  let data = {
    labels,
    datasets: [
      {
        label: "Tài khoản mới",
        data: months?.totalAccounts || [5, 4, 1],
        backgroundColor: "rgba(53, 162, 235, 0.5)",
      },
      {
        label: "Tiểu thương mới",
        data: months?.totalStoreOwners || [10, 20, 30],
        backgroundColor: "rgba(53, 162, 235, 1)",
      },
    ],
  };

  // let orderOptions = {
  //   responsive: true,
  //   plugins: {
  //     legend: {
  //       position: "top",
  //     },
  //     title: {
  //       display: true,
  //       text: "Thống kê đơn hàng",
  //     },
  //   },
  // };

  // let orderData = {
  //   labels,
  //   datasets: [
  //     {
  //       label: "Đơn hàng mới",
  //       data: months?.totalOrders || [5, 4, 1],
  //       backgroundColor: "rgba(0, 162, 235, 0.5)",
  //     },
  //   ],
  // };

  let revenueOptions = {
    responsive: true,
    plugins: {
      legend: {
        position: "top",
      },
      title: {
        display: true,
        text: "Thống kê lợi nhuận, lượt mua thành viên",
      },
    },
  };

  let revenueData = {
    labels,
    datasets: [
      {
        label: "Lượt mua tiểu thương",
        data: months?.totalRegistrations || [5, 4, 1],
        backgroundColor: "rgba(255,0,255, 0.5)",
      },
      {
        label: "Doanh thu",
        data: months?.totalRegistrationAmount || [10, 20, 30],
        backgroundColor: "rgba(255,0,0, 0.5)",
      },
    ],
  };

  return (
    <>
      <div className="container-fluid pt-4 px-4">
        <div className="row g-4" style={{ justifyContent: "center" }}>
          <div className="col-sm-6 col-xl-4">
            <div className="bg-light rounded d-flex align-items-center justify-content-between p-4">
              <i className="fa fa-chart-line fa-3x text-primary"></i>
              <div className="ms-3">
                <p className="mb-2">Tổng tài khoản</p>
                <h6 className="mb-0">{dashboard?.totalAccounts}</h6>
              </div>
            </div>
          </div>
          <div className="col-sm-6 col-xl-4">
            <div className="bg-light rounded d-flex align-items-center justify-content-between p-4">
              <i className="fa fa-chart-bar fa-3x text-primary"></i>
              <div className="ms-3">
                <p className="mb-2">Tổng số tiểu thương</p>
                <h6 className="mb-0">{dashboard?.totalStoreOwners}</h6>
              </div>
            </div>
          </div>
          <div className="col-sm-6 col-xl-4">
            <div className="bg-light rounded d-flex align-items-center justify-content-between p-4">
              <i className="fa fa-chart-area fa-3x text-primary"></i>
              <div className="ms-3">
                <p className="mb-2">Tổng số lượt mua gói tiểu thương</p>
                <h6 className="mb-0">{dashboard?.totalRegistrations}</h6>
              </div>
            </div>
          </div>
          <div className="col-sm-6 col-xl-4">
            <div className="bg-light rounded d-flex align-items-center justify-content-between p-4">
              <i className="fa fa-chart-area fa-3x text-primary"></i>
              <div className="ms-3">
                <p className="mb-2">Doanh thu lượt mua gói tiểu thương</p>
                <h6 className="mb-0">
                  {CommonService.formatNumberWithDots(
                    dashboard?.totalRegistrationAmount * 1
                  )}{" "}
                  VND
                </h6>
              </div>
            </div>
          </div>
          <div className="col-sm-6 col-xl-4">
            <div className="bg-light rounded d-flex align-items-center justify-content-between p-4">
              <i className="fa fa-chart-area fa-3x text-primary"></i>
              <div className="ms-3">
                <p className="mb-2">Lượt mua tiểu thương (hôm nay)</p>
                <h6 className="mb-0">{todayData?.selling}</h6>
              </div>
            </div>
          </div>
          <div className="col-sm-6 col-xl-4">
            <div className="bg-light rounded d-flex align-items-center justify-content-between p-4">
              <i className="fa fa-chart-area fa-3x text-primary"></i>
              <div className="ms-3">
                <p className="mb-2">Doanh thu lượt mua (hôm nay)</p>
                <h6 className="mb-0">
                  {CommonService.formatNumberWithDots(todayData?.revenue * 1)}{" "}
                  VND
                </h6>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div
        style={{
          margin: "20px 0",
          justifyContent: "center",
          display: "flex",
          marginTop: "40px",
        }}
      >
        <input
          style={{ marginRight: "10px" }}
          type="month"
          defaultValue={selectMonth}
          ref={monthRef}
        />
        <button onClick={handleSearchMonth} className="btn btn-primary">
          Tìm kiếm
        </button>
      </div>
      <div style={{ marginTop: "20px", display: "flex", flexWrap: "wrap" }}>
        <div style={{ width: "48%", margin: "0 1%" }}>
          <Bar options={options} data={data} />
        </div>
        {/* <div style={{ width: "48%", margin: "0 1%" }}>
          <Bar options={orderOptions} data={orderData} />
        </div> */}
        <div style={{ width: "48%", margin: "0 1%", marginTop: "20px" }}>
          <Bar options={revenueOptions} data={revenueData} />
        </div>
      </div>
    </>
  );
};

export default Dashboard;
